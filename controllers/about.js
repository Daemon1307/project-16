

const express = require('express')
const api = express.Router()
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------
api.get('/DerickMcCrary', (req, res) => {
  res.render('about/Derick.ejs')
})
api.get('/bogarajula', (req, res) => {
  res.render('about/bogarajula.ejs')
})
api.get('/Yanala', (req, res) => {
  res.render('about/Yanala.ejs')
})


module.exports = api
